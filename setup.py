#!/usr/bin/env python

from setuptools import setup

import kepcobop


def main():

    setup(
        name='kepcobop',
        version=kepcobop.__version__,
        package_dir={'kepcobop': 'kepcobop'},
        packages=['kepcobop'],
        include_package_data=True,  # include files in MANIFEST
        author='Jairo Moldes',
        author_email='jmoldes@cells.es',
        description='Device server for controlling a Kepco BOP power supply.',
        license='GPLv3+',
        platforms=['src', 'noarch'],
        url='https://git.cells.es/controls/kepcobop.git',
        requires=['tango (>=7.2.6)'],
        entry_points={
            'console_scripts': [
                'KepcoBOP = kepcobop.KepcoBOP:main'
            ],
        },
    )


if __name__ == "__main__":
    main()
